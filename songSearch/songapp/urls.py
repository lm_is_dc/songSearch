# encoding: utf-8
'''
@license: (C) Copyright 2018-2017, Node Supply Chain Manager Corporation Limited.
@contact: 1257309054@qq.com
@file: urls.py
@time: 2018/8/13 16:09
@author:LDC
'''


from django.urls import path, include
from songapp import views

app_name ='songapp'

urlpatterns = (
    # 添加路由
    path(r'login',views.login,name='login'),

)