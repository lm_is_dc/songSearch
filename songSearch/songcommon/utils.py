# encoding: utf-8
'''
@license: (C) Copyright 2018-2017, Node Supply Chain Manager Corporation Limited.
@contact: 1257309054@qq.com
@file: utils.py
@time: 2018/8/12 20:50
@author:LDC
'''
import requests
import re

s = requests.session()

def get_url(base_url):
    # print('base_url',base_url)
    base_number = re.findall(r"(?:http://www.kuwo.cn/yinyue/)(\d+)(?:\?catalog=yueku2016)?", base_url)[0]

    headers = {

        'Accept-Encoding': 'identity;q=1, *;q=0',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
        'Connection': 'keep-alive',
        'Host':'antiserver.kuwo.cn',
        'Range': 'bytes=0-',
        'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Referer':base_url
    }
    url = "http://antiserver.kuwo.cn/anti.s?format=aac|mp3&rid=MUSIC_"+ base_number +"&type=convert_url&response=res"
    #      http://win.web.nf03.sycdn.kuwo.cn/bac2d366fcf102fa2fbea0981f91f750/5b6d9b6d/resource/a3/5/96/341754394.aac
    # print(url)
    r = s.get(url, headers=headers, allow_redirects=False)
    # print(r.headers['Location'])
    return r.headers['Location']

def main():
    base_url = input("请输入音乐播放页面url：").strip()
    # print(get_url(base_url))

if __name__ == '__main__':
    main()