
from django.core.cache import cache
from django.http import HttpResponse
from django.shortcuts import render

from songcommon.models import Song
from songcommon.utils import get_url




def home(request):
    return render(request,'search/search.html',context={'search_method':'songname'})



def search_song(request,sid):
    s = Song.objects.get(sid=sid)
    # print('********',s)
    # print('===========',s.url)
    # print(get_url(s.url))

    s.playnum += 1
    s.save()
    # print(s.lyric.split('\n'))
    back_url = cache.get('host:' + request.get_host() + 'url')

    context = {'name':s.name,'url': get_url(s.url),'lyrics':s.lyric.split('\n'),'back_url':back_url}
    return render(request, 'search/song.html',context=context)


