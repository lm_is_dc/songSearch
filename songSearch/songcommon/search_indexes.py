# encoding: utf-8
'''
@license: (C) Copyright 2018-2017, Node Supply Chain Manager Corporation Limited.
@contact: 1257309054@qq.com
@file: search_indexes.py
@time: 2018/8/12 16:07
@author:LDC
'''

from haystack import indexes
#引入你项目下的model（也就是你要将其作为检索关键词的models）
from songcommon.models import Song

# model名 + Index作为类名
class SongIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    lyric = indexes.CharField(model_attr='lyric')

    def get_model(self):
        return Song

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

