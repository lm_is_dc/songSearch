# encoding: utf-8
'''
@license: (C) Copyright 2018-2017, Node Supply Chain Manager Corporation Limited.
@contact: 1257309054@qq.com
@file: urls.py
@time: 2018/8/13 16:08
@author:LDC
'''

from django.urls import   path

from songcommon import views
from songcommon.searchView import MySearchView

app_name ='songcommon'

urlpatterns = (
    path(r'<int:sid>', views.search_song, name='search_song'),
    path(r'search/?', MySearchView.as_view(), name='haystack_search'),

)