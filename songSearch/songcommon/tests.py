from django.test import TestCase
import re
# Create your tests here.
import pymysql

if __name__ == '__main__':
    #连接数据库
    con_localhost = pymysql.connect(
        host='localhost', #数据库所在地址URL
        user='root', #用户名
        password='123456', #密码
        database='songsearch', #数据库名称
        port=3306,  #端口号
        charset='utf8'
    )
    #拿到查询游标
    cursor_localhost = con_localhost.cursor()
    #使用游标执行SQL语句
    with open(r'D:\Learn\pythonPro\qianfeng\Song.sql','r',encoding='utf-8') as f:
        sqls = f.readlines()
        for sql in sqls:
            # print(sql)
            sid = re.findall('VALUES \((\d+)\)',sql)
            if not cursor_localhost.execute('select sid from Song where sid = %d' %sid[0]):
                affected = cursor_localhost.execute(sql)
                print("插入成功，affected=",affected)
                # 如果是增删改需要提交
                con_localhost.commit()
#断开连接
con_localhost.close()