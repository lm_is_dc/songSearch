# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

# 评论表
class Comment(models.Model):
    id = models.IntegerField(primary_key=True)
    uid = models.IntegerField(verbose_name='用户id')
    sid = models.IntegerField(verbose_name='歌曲id')
    content = models.TextField(verbose_name='评论内容')
    replynum = models.IntegerField(db_column='replyNum', blank=True, null=True,verbose_name='回复数量')  
    likenum = models.IntegerField(db_column='likeNum', blank=True, null=True,verbose_name='点赞数量')  
    createtime = models.DateTimeField(db_column='createTime',auto_now_add=True, blank=True, null=True,verbose_name='创建时间')  
    isdelete = models.IntegerField(db_column='isDelete', blank=True, null=True,verbose_name='是否删除')  
    replyid = models.IntegerField(db_column='replyId', blank=True, null=True,verbose_name='回复哪条评论id')  

    class Meta:
        managed = False
        db_table = 'Comment'

# 歌曲表
class Song(models.Model):
    sid = models.AutoField(primary_key=True,verbose_name='歌曲编号')
    name = models.CharField(max_length=100,verbose_name='歌名')
    singer = models.CharField(max_length=100, blank=True, null=True,verbose_name='歌手')
    lyric = models.TextField(blank=True, null=True,verbose_name='歌词')
    url = models.TextField(verbose_name='歌曲url')
    songdata = models.TextField(db_column='songData', blank=True, null=True,verbose_name='歌曲文件')
    likenum = models.IntegerField(db_column='likeNum', blank=True, null=True,verbose_name='点赞数')
    playnum = models.IntegerField(db_column='playNum', blank=True, null=True,verbose_name='播放量')
    commentnum = models.IntegerField(db_column='commentNum', blank=True, null=True,verbose_name='评论数')
    iscopyright = models.IntegerField(db_column='isCopyright', blank=True, null=True,verbose_name='版权信息')
    quality = models.CharField(max_length=10, blank=True, null=True,verbose_name='音质')
    typeid = models.IntegerField(db_column='typeId', blank=True, null=True,verbose_name='歌曲类型id')
    originid = models.IntegerField(db_column='originId', blank=True, null=True,verbose_name='歌曲来源id')

    class Meta:
        managed = False
        db_table = 'Song'
        ordering = ['-playnum', ]
        unique_together = (('name', 'singer', 'originid'),)

# 歌曲来源表
class Songorigin(models.Model):
    originid = models.IntegerField(db_column='originId', primary_key=True,verbose_name='歌曲来源id')
    origin = models.CharField(max_length=10,verbose_name='来源')
    isdelete = models.IntegerField(db_column='isDelete', blank=True, null=True,verbose_name='是否删除')

    class Meta:
        managed = False
        db_table = 'SongOrigin'

# 歌曲分类表
class Songtype(models.Model):
    typeid = models.IntegerField(db_column='typeId', primary_key=True,verbose_name='歌曲分类id')
    type = models.CharField(max_length=10,verbose_name='歌曲类型')
    isdelete = models.IntegerField(db_column='isDelete', blank=True, null=True,verbose_name='是否删除')

    class Meta:
        managed = False
        db_table = 'SongType'

# 用户信息表
class User(models.Model):
    uid = models.AutoField(primary_key=True,verbose_name='用户id')
    name = models.CharField(db_column='NAME', unique=True, max_length=20,verbose_name='用户名')
    password = models.CharField(db_column='PASSWORD', max_length=32,verbose_name='密码')
    level = models.IntegerField(db_column='LEVEL', blank=True, null=True,verbose_name='等级')
    phone = models.IntegerField(verbose_name='手机号')
    icon = models.ImageField(blank=True, null=True,verbose_name='头像')
    signature = models.CharField(max_length=200, blank=True, null=True,verbose_name='签名')
    isdelete = models.IntegerField(db_column='isDelete', blank=True, null=True,verbose_name='是否删除')
    createtime = models.DateTimeField(db_column='createTime',auto_now_add=True, blank=True, null=True,verbose_name='创建时间')
    createid = models.IntegerField(db_column='createId', blank=True, null=True,verbose_name='创建人id')
    modifytime = models.DateTimeField(db_column='modifyTime', blank=True, null=True,verbose_name='修改时间')
    modifyid = models.IntegerField(db_column='modifyId', blank=True, null=True,verbose_name='修改人id')
    issilent = models.IntegerField(db_column='isSilent', blank=True, null=True,verbose_name='是否禁言')

    class Meta:
        managed = False
        db_table = 'User'

# 用户与歌曲关系表
class UserToSong(models.Model):
    id = models.IntegerField(primary_key=True,verbose_name='id')
    uid = models.IntegerField(blank=True, null=True,verbose_name='用户id')
    sid = models.IntegerField(blank=True, null=True,verbose_name='歌曲id')
    playtime = models.DateTimeField(db_column='playTime', auto_now_add=True,blank=True, null=True,verbose_name='播放时间')
    iscollect = models.IntegerField(db_column='isCollect', blank=True, null=True,verbose_name='是否收藏')
    playcount = models.IntegerField(db_column='playCount', blank=True, null=True,verbose_name='播放次数')
    iscomment = models.IntegerField(db_column='isComment', blank=True, null=True,verbose_name='评论次数')

    class Meta:
        managed = False
        db_table = 'User_to_Song'

# 用户与用户关系表
class UserToUser(models.Model):
    uid = models.IntegerField(verbose_name='用户id')
    attentionuid = models.IntegerField(db_column='attentionUid',verbose_name='被关注的用户id')
    isdelete = models.IntegerField(db_column='isDelete', blank=True, null=True,verbose_name='是否删除')

    class Meta:
        managed = False
        db_table = 'User_to_User'

