
from xadmin import views
import xadmin
from .models import Song
# 基础设置
class BaseSetting(object):
    enable_themes = True    # 使用主题
    use_bootswatch = True


# 全局设置
class GlobalSettings(object):
    site_title = '音乐搜索引擎'  # 标题
    site_footer = '音乐搜索引擎'  # 页尾
    site_url = '/'
    menu_style = 'accordion'  # 设置左侧菜单  折叠样式

# 用户的后台管理
class SongAdmin(object):
    # 检索字段
    search_fields = ['name','singer',]
    # 要显示的字段
    list_display = ['sid','name','singer','lyric','url',]
    # 分组过滤的字段
    list_filter = ['name','singer','lyric','url',]
    # ordering设置默认排序字段，负号表示降序排序
    ordering = ('sid',)
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 50
    # list_editable 设置默认可编辑字段
    list_editable = ['name', 'singer']

xadmin.site.register(views.CommAdminView,GlobalSettings)
xadmin.site.register(views.BaseAdminView,BaseSetting)

xadmin.site.register(Song, SongAdmin)







