# songSearch


- 项目
  - 音乐搜索引擎
- 全局搜索设置可参考博客
  - https://blog.csdn.net/lm_is_dc/article/details/82053012
- 描述
  - 用户输入关键词(歌手、歌名、歌词)来搜索音乐，把搜索的结果按热度排序，把前5个结果返回给用户，用户可以点击播放按钮播放音乐
- 功能
  - 搜索音乐、评论、点赞、收藏、下载、登录、注册、发表主题、音乐分类等
- 技术
  - web前后端、爬虫、数据分析、机器学习
- 框架
  - Django、Scripy、TensorFlow
